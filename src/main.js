import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;
fetch("/cfg/settings.json")
  .then((config) => config.json())
  .then((config) => {
    new Vue({
      data: () => ({
        config: config,
      }),
      router,
      store,
      render: (h) => h(App),
    }).$mount("#app");
  });
